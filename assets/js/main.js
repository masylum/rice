var app = new Vue({
  el: '#app',
  data: {
    score: '560',
    reach: {
      number: 10
    },
    impact: {
      number: 0.5
    },
    confidence: {
      number: 0
    }
  },
  methods: {
    computeScore: function () {
      this.score = Math.floor( this.reach.number * this.impact.number * this.confidence.number );
    }
  },
  mounted: function () {
    this.computeScore();
  }
})